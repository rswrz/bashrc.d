#!/bin/bash
for module in ~/.bashrc.d/modules/*; do
  file=$module/main.sh
  if [ -f $module ] && [ ${module: -3} == ".sh" ]; then
	  source $module
  elif [ -d $module ] && [ -f $module/main.sh ]; then
	  source $module/main.sh
  fi
done
