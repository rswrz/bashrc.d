#!/bin/bash

# docker-machine create $DEFAULTS
VIRTUALBOX_MEMORY_SIZE=4096
VIRTUALBOX_CPU_COUNT=2
VIRTUALBOX_SHARE_FOLDER=~/Workspace/minidock:workspace

function minidock {
  docker-machine "${@}" minidock
}

minidock status | grep Started > /dev/null 2>&1 && eval $(minidock env)

alias dock=minidock
